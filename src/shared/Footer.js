import React, { Component } from 'react';

class Footer extends Component {
    render() {
        return <footer className="footer mt-auto py-3">
                <div className="container">
                    <span>Interested in Fear and Terror? Check out our <a href="https://discord.gg/HDXbpX8">Discord</a>.</span>
                </div>
            </footer>
    }
}

export default Footer;