import React, { Component } from 'react';
import questItems from '../assets/questitems.jpg';

class Quests extends Component {
    render() {
        return <div className="container">
                    <div className="mb-4">
                        <h5>Quest Item Requests</h5>
                        <p>Enter your name and the items you need in the spreadsheet linked below, then post in #tarkov to let us know there's a new entry.</p>
                        <a href="https://docs.google.com/spreadsheets/d/1l7zjnDDa31CLE51DyHya9_L5QwrOOYpgKpXDDA2mmH4/edit?usp=sharing">Quest Item Spreadsheet</a>
                    </div>
                    <div className="card" id="cardAccordion">
                        <div className="card-header">
                            <h2 className="mb-0">
                                <button className="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseQuestItems" aria-expanded="false" aria-controls="collapseQuestItems">
                                Quest Items
                                </button>
                            </h2>
                        </div>
                        <div id="collapseQuestItems" className="collapse" aria-labelledby="headingQuestItems" data-parent="#cardAccordion">
                            <div className="card-body">
                                <p>This quest item sheet was provided by a community member on /r/escapefromtarkov</p>
                                <img className="w-100 m-2" src={questItems} alt="Quest Items" />
                            </div>
                        </div>
                    </div>
                </div>
    }
}

export default Quests;