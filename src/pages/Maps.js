import React, { Component } from 'react';
import shorelineMap from '../assets/maps/ShorelineMapSpawnsExitsKeys.jpg';
import shorelineResortMap from '../assets/maps/ResortRoomsNew.jpg';
import customs3D from '../assets/maps/Customs_3d_Full.jpg';
import customsCallouts from '../assets/maps/Customs_callouts_1080.jpg';
import woodsMap from '../assets/maps/WoodsMapKeySpawnsExits.jpg';
import factoryMap from '../assets/maps/FactoryCalloutsMap.jpg';
import interchangeMap from '../assets/maps/InterchangeMapLorathor.jpg';
import labsMap from '../assets/maps/LabsMap.jpg'

class Maps extends Component {
    render() {
        return <div className="container">
            <p>[FaT] did not create any of these maps. They have been distributed in /r/escapefromtarkov and were posted in our discord.
                 This page is simply a convenience for [FaT] members.</p>
                <div className="accordion" id="mapsAccordion">
                    <div className="card">
                        <div className="card-header">
                            <h2 className="mb-0">
                                <button className="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseShoreline" aria-expanded="false" aria-controls="collapseShoreline">
                                Shoreline
                                </button>
                            </h2>
                        </div>
                        <div id="collapseShoreline" className="collapse" aria-labelledby="headingShoreline" data-parent="#mapsAccordion">
                            <div className="card-body">
                                <img className="w-100 m-2" src={shorelineMap} alt="Shoreline Map" />
                                <img className="w-100 m-2" src={shorelineResortMap} alt="Shoreline Resort Map" />
                            </div>
                        </div>
                    </div>
                    <div className="card">
                        <div className="card-header">
                            <h2 className="mb-0">
                                <button className="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseCustoms" aria-expanded="false" aria-controls="collapseCustoms">
                                Customs
                                </button>
                            </h2>
                        </div>
                        <div id="collapseCustoms" className="collapse" aria-labelledby="headingCustoms" data-parent="#mapsAccordion">
                            <div className="card-body">
                                <div class="alert alert-warning" role="alert">
                                These Customs maps do not include the new expansion area behind Gas Station.
                                </div>
                                <img className="w-100 m-2" src={customs3D} alt="Customs Map" />
                                <img className="w-100 m-2" src={customsCallouts} alt="Customs 2 Map" />
                            </div>
                        </div>
                    </div>
                    <div className="card">
                        <div className="card-header">
                            <h2 className="mb-0">
                                <button className="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseWoods" aria-expanded="false" aria-controls="collapseWoods">
                                Woods
                                </button>
                            </h2>
                        </div>
                        <div id="collapseWoods" className="collapse" aria-labelledby="headingWoods" data-parent="#mapsAccordion">
                            <div className="card-body">
                                <img className="w-100 m-2" src={woodsMap} alt="Woods Map" />
                            </div>
                        </div>
                    </div>
                    <div className="card">
                        <div className="card-header">
                            <h2 className="mb-0">
                                <button className="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseFactory" aria-expanded="false" aria-controls="collapseFactory">
                                Factory
                                </button>
                            </h2>
                        </div>
                        <div id="collapseFactory" className="collapse" aria-labelledby="headingFactory" data-parent="#mapsAccordion">
                            <div className="card-body">
                                <img className="w-100 m-2" src={factoryMap} alt="Factory Map" />
                            </div>
                        </div>
                    </div>
                    <div className="card">
                        <div className="card-header">
                            <h2 className="mb-0">
                                <button className="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseInterchange" aria-expanded="false" aria-controls="collapseInterchange">
                                Interchange
                                </button>
                            </h2>
                        </div>
                        <div id="collapseInterchange" className="collapse" aria-labelledby="headingInterchange" data-parent="#mapsAccordion">
                            <div className="card-body">
                                <img className="w-100 m-2" src={interchangeMap} alt="Interchange Map" />
                            </div>
                        </div>
                    </div>
                    <div className="card">
                        <div className="card-header">
                            <h2 className="mb-0">
                                <button className="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseLabs" aria-expanded="false" aria-controls="collapseLabs">
                                Labs
                                </button>
                            </h2>
                        </div>
                        <div id="collapseLabs" className="collapse" aria-labelledby="headingLabs" data-parent="#mapsAccordion">
                            <div className="card-body">
                                <img className="w-100 m-2" src={labsMap} alt="Labs Map" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    }
}

export default Maps;