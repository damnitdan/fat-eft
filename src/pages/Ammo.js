import React, { Component } from 'react';

class Ammo extends Component {
    render() {
        return <div className="container">
            <div className="row">
                <div className="card col-4">
                    <div className="card-body">
                        <h5 className="card-title">Ammo Spreadsheet</h5>
                        <p className="card-text">The below spreadsheet offers an indepth view of how ammo in EFT works, including how they work against armor and more.</p>
                        <a href="https://docs.google.com/spreadsheets/d/1jjWcIue0_PCsbLQAiL5VrIulPK8SzM5jjiCMx9zUuvE/htmlview?sle=true#gid=64053005" className="card-link">NoFoodAfterMidnight's Ammo and Armor</a>
                    </div>
                </div>
                <div className="card col-8">
                    <div className="card-body">
                        <h5 className="card-title">About Ammo</h5>
                        <p className="card-text">In general, for rifle calibers you want to use ammo with a higher armor pen. 
                        For pistol calibers, you want to use ammo that does more flesh damage, since you won't be able to penetrate armor. 
                        This means with pistol calibers you'll be aiming for the head and limbs, where there is no armor. 
                        In the below list, you'll want to choose the ammo furthest to the right that you can purchase.</p>
                        <ul>
                            <li>5.45x39: PS -> BP -> BT -> BS</li>
                            <li>7.62x39: PS -> BP</li>
                            <li>5.56x45: M855A1 -> M856A1 -> M995</li>
                            <li>9x19: PSO GZH -> Luger CCI</li>
                            <li>9x18: PMM -> PSV -> SP8 -> SP7</li>
                            <li>7.62x25: FMJ43 -> LRNPC</li>
                            <li>7.62x51: M80 -> M62 -> M61</li>
                            <li>7.62x54: LPS GZH -> SNB</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    }
}

export default Ammo;