import React, { Component } from 'react';
import fatlogo from '../assets/Logo2dark.png';

class Home extends Component {
    render() {
        return <div className="container">
                <div className="jumbotron">
                    <h1 className="display-4">Fear and Terror</h1>
                    <p className="lead">Fear and Terror is an online community that focuses on playing games that require Teamwork and Communication. 
One of our biggest draws is that we heavily promote activity. With one Director leading each game, we have popular communities in all the games we expand in!</p>
                    <hr className="my-4" />
                    <img src={fatlogo} className="rounded mx-auto d-block w-25 p-3" alt="Fear And Terror Logo" />
                </div>
            </div>
    }
}

export default Home;