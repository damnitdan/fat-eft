import React, { Component } from 'react';
import { HashRouter, Switch, Route } from 'react-router-dom';
import Header from './shared/Header';
import Footer from './shared/Footer';
import Home from './pages/Home';
import Maps from './pages/Maps';
import Quests from './pages/Quests';
import Ammo from './pages/Ammo';
import Guns from './pages/Guns';

class App extends Component {
  render() {
    return (
        <HashRouter>
          <div className="container">
            <Header/>
            <div className="my-3"> 
              <Switch>
                <Route path="/maps" component={Maps} />
                <Route path="/quests" component={Quests} />
                <Route path="/ammo" component={Ammo} />
                <Route path="/guns" component={Guns} />
                <Route path="/" component={Home} />
              </Switch>
            </div>
            <Footer/>
          </div>
        </HashRouter>
    );
  }
}

export default App;
